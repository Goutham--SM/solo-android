package com.example.goutham.solo;

import android.net.Uri;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

public class ActiveUser {
    static int ids;
    static String names,emails,dob,username,address,pass,contact;



    public void setPass(String pass) {
        this.pass = pass;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public void setEmails(String emails) {
        this.emails = emails;
    }

    public void setIds(int ids) {
        this.ids = ids;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public static int getIds() {
        return ids;
    }

    public static String getContact() {
        return contact;
    }

    public static String getPass() {
        return pass;
    }

    public static String getAddress() {
        return address;
    }

    public static String getDob() {
        return dob;
    }

    public static String getEmails() {
        return emails;
    }

    public static String getNames() {
        return names;
    }

    public static String getUsername() {
        return username;
    }
}
