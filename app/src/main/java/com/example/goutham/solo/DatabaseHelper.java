package com.example.goutham.solo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import static com.example.goutham.solo.ActiveUser.*;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "User.db";
    public static final String TABLE_NAME = "personalinfo";
    public static final String NAME = "name";
    public static final String EMAIL = "email";
    public static final String USERNAME = "username";
    public static final String ADDRESS = "address";
    public static final String DOB = "dob";
    public static final String PHONE = "phone";
    public static final String PASS = "pass";
    public static final String ID = "id";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
        SQLiteDatabase db=this.getWritableDatabase();
    }

    public Cursor getProfile(){
        SQLiteDatabase db= this.getWritableDatabase();
        Cursor cr = db.rawQuery("select * from "+TABLE_NAME+" where "+EMAIL+" = ? ",new String[] {ActiveUser.getEmails()});
        return cr;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS "+TABLE_NAME+ "("+ID+" INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, "+NAME+" TEXT, "+EMAIL+" TEXT UNIQUE, "+USERNAME+" TEXT , "+ADDRESS+" TEXT, "+PHONE+" INTEGER, "+DOB+",INTEGER," +PASS+" TEXT );");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(db);
    }

    public boolean insertsign(String email, String password){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv= new ContentValues();
        cv.put(EMAIL,email);
        cv.put(PASS,password);

        long res = db.insert(TABLE_NAME,null,cv);
        if(res==-1)
            return false;
        else
            return true;
    }

    public boolean insertProfile(String usname, String address, String contact, String name, String dob, String pass) throws ParseException {
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues cv =new ContentValues();
        Date sdf =new SimpleDateFormat("dd/MM/yyyy").parse(dob);

        cv.put(USERNAME,usname);
        cv.put(ADDRESS,address);
        cv.put(PHONE,contact);
        cv.put(NAME,name);
        cv.put(DOB,sdf.toString());
        if(!(pass.isEmpty()||TextUtils.isEmpty(pass)||pass==null||pass.equals(null)||pass.equals(""))) {
            cv.put(PASS, pass);
        }else {
            cv.put(PASS,ActiveUser.getPass());
        }
        String s="email = ?";
        long res = db.update(TABLE_NAME,cv,s,new String[]{ ActiveUser.getEmails()});
        if(res==-1)
            return false;
        else
            return true;
    }
}
