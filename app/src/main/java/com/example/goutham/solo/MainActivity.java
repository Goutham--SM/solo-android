package com.example.goutham.solo;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;


import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import static com.example.goutham.solo.DatabaseHelper.EMAIL;
import static com.facebook.FacebookSdk.getApplicationContext;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    GoogleSignInClient mGoogleSignInClient;
    Dialog md;
    DatabaseHelper mdb;


    ActiveUser ac =new ActiveUser();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        signOut();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this);
        if (acct != null) {
            String personName = acct.getDisplayName();
            String personGivenName = acct.getGivenName();
            String personEmail = acct.getEmail();
            String personId = acct.getId();

            ac.setNames(personName);
            ac.setUsername(personGivenName);
            ac.setEmails(personEmail);

            SQLiteDatabase dbs = mdb.getWritableDatabase();
            Cursor cr = dbs.rawQuery("SELECT * FROM personalinfo WHERE " + EMAIL + " = ? ", new String[]{personEmail});
            cr.moveToFirst();
            if (cr.getCount()<=0) {
                boolean isinserted = mdb.insertsign(personEmail, " ");
            }

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setCheckedItem(R.id.home);
        getSupportFragmentManager().beginTransaction().replace(R.id.frag_container, new about()).commit();
        navigationView.setNavigationItemSelectedListener(this);
        hideItem();
    }

    private void hideItem()
    {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.profile).setVisible(false);
        nav_Menu.findItem(R.id.cd).setVisible(false);
        nav_Menu.findItem(R.id.signout).setVisible(false);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            md=new Dialog(this);
            ImageView cls;
            md.setContentView(R.layout.contact_us);
            cls = (ImageView) md.findViewById(R.id.cls);
            cls.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    md.dismiss();
                }
            });
            Button btn =md.findViewById(R.id.button);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_TEXT, "Please change the current plan of " + ActiveUser.getEmails()+" "+ActiveUser.getNames());
                    if(intent.resolveActivity( getPackageManager() )!=null)
                        startActivity(intent);

                }
            });
            md.show();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.profile) {
            getSupportFragmentManager().beginTransaction().replace(R.id.frag_container, new profile()).commit();
        } else if (id == R.id.home) {
            getSupportFragmentManager().beginTransaction().replace(R.id.frag_container, new about()).commit();

        } else if (id == R.id.product) {
            getSupportFragmentManager().beginTransaction().replace(R.id.frag_container, new product()).commit();
        } else if (id == R.id.cd) {
            if(ActiveUser.getEmails().equals("asd@gmail.com")){
                getSupportFragmentManager().beginTransaction().replace(R.id.frag_container, new devices()).commit();
            }else {
                Toast.makeText(this, "Buy to view Connected devices", Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.signin) {
            getSupportFragmentManager().beginTransaction().replace(R.id.frag_container, new signin()).commit();

        } else if (id == R.id.signout) {
            hideItem();
            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setCheckedItem(R.id.home);

            getSupportFragmentManager().beginTransaction().replace(R.id.frag_container, new about()).commit();
            Menu nav_Menu = navigationView.getMenu();
            nav_Menu.findItem(R.id.signin).setVisible(true);
            signOut();
        } else if (id == R.id.about) {
            getSupportFragmentManager().beginTransaction().replace(R.id.frag_container, new home()).commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    private void signOut() {

        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        ac.setIds(0);
                        ac.setEmails(null);
                        ac.setUsername(null);
                        ac.setNames(null);
                        ac.setDob(null);
                        ac.setContact(null);
                        ac.setAddress(null);
                        Toast.makeText(MainActivity.this,"Signed out",Toast.LENGTH_LONG).show();
                        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
                        navigationView.setCheckedItem(R.id.home);

                        getSupportFragmentManager().beginTransaction().replace(R.id.frag_container, new about()).commit();
                        Menu nav_Menu = navigationView.getMenu();
                        nav_Menu.findItem(R.id.signin).setVisible(true);
                    }
                });
        LoginManager.getInstance().logOut();
    }
}
