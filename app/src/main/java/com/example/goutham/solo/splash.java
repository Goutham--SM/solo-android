package com.example.goutham.solo;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.goutham.solo.MainActivity;

public class splash extends AppCompatActivity {

    private static int SPLASH_SCREEN=5500;
    Animation topanim, fade,faded,fadedd,fadeddd;
    ImageView house,lefts,rights,legs,heart;
    TextView cap;

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splash);

        topanim=AnimationUtils.loadAnimation(this,R.anim.topanim);
        fade=AnimationUtils.loadAnimation(this,R.anim.fadeanim);
        faded=AnimationUtils.loadAnimation(this,R.anim.fadeanimd);
        fadedd=AnimationUtils.loadAnimation(this,R.anim.fadeanimdd);
        fadeddd=AnimationUtils.loadAnimation(this,R.anim.fadeanimddd);

        house=findViewById(R.id.house);
        lefts=findViewById(R.id.left);
        rights=findViewById(R.id.right);
        legs=findViewById(R.id.legs);
        heart=findViewById(R.id.heart);
        cap=findViewById(R.id.cation);

        house.setAnimation(faded);
        heart.setAnimation(fade);
        lefts.setAnimation(fadeddd);
        rights.setAnimation(fadeddd);
        legs.setAnimation(fadedd);
        cap.setAnimation(topanim);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(splash.this,MainActivity.class);
                startActivity(i);
                finish();
            }
        },SPLASH_SCREEN);


    }
}
