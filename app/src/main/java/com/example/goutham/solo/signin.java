package com.example.goutham.solo;

import android.app.Dialog;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.LoginStatusCallback;
import com.facebook.appevents.AppEventsLogger;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.Arrays;

import static android.app.Activity.RESULT_OK;
import static android.support.constraint.motion.utils.Oscillator.TAG;
import static com.example.goutham.solo.DatabaseHelper.EMAIL;
import static com.example.goutham.solo.DatabaseHelper.ID;
import static com.example.goutham.solo.DatabaseHelper.NAME;
import static com.example.goutham.solo.DatabaseHelper.PASS;
import static com.example.goutham.solo.ActiveUser.*;
import static com.facebook.FacebookSdk.getApplicationContext;
import static com.facebook.places.model.PlaceFields.PICTURE;

public class signin extends Fragment {
    Dialog md;
    Bitmap mIcon1 = null;
    String imgUrl = "";
    String id = "";
    String name = "";
    String email = "";
    CallbackManager callbackManager;
    DatabaseHelper mdb;
    EditText et1, et2,et,et3,et4;
    Button signupbut;
    ImageView gl;
    LoginButton loginButton;
    GoogleSignInClient mGoogleSignInClient;
    private TextInputLayout textEmail,textpass,textpass1,emails,passw;
    ActiveUser ac = new ActiveUser();
    int RC_SIGN_IN=0;

    private static final String FIELDS = "fields";

    private static final String REQUEST_FIELDS =
            TextUtils.join(",", new String[]{ID, NAME, EMAIL});

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.signin,container,false);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);
        callbackManager = CallbackManager.Factory.create();


        gl= v.findViewById(R.id.gl);

        loginButton = (LoginButton) v.findViewById(R.id.fb);
        loginButton.setReadPermissions(Arrays.asList("public_profile"));
        loginButton.setFragment(this);

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code

                Toast.makeText(getContext(),"Signed in with facebook, Welcome!",Toast.LENGTH_LONG).show();
                Fragment fragment = new home();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frag_container, fragment);
                fragmentTransaction.commit();

                NavigationView navigationView = (NavigationView) getActivity().findViewById(R.id.nav_view);
                Menu nav_Menu = navigationView.getMenu();
                navigationView.setCheckedItem(R.id.home);
                nav_Menu.findItem(R.id.profile).setVisible(true);
                nav_Menu.findItem(R.id.cd).setVisible(true);
                nav_Menu.findItem(R.id.signout).setVisible(true);
                nav_Menu.findItem(R.id.signin).setVisible(false);
                fetchUserInfo();
            }

            @Override
            public void onCancel() {
                // App code
                Toast.makeText(getActivity(), "cancel!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Toast.makeText(getActivity(), "Err!", Toast.LENGTH_SHORT).show();
            }
        });
        // Callback registration




        gl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.gl:
                        signIn();
                        break;
                    // ...
                }
            }
        });

        md=new Dialog(getActivity());
        mdb = new DatabaseHelper(getActivity());

        passw = v.findViewById(R.id.editText2);
        emails = v.findViewById(R.id.editText);

        Button btn = v.findViewById(R.id.button);
        btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!validateEmails() | !validatePassw()) {
                    return;
                } else {
                    SQLiteDatabase dbs = mdb.getWritableDatabase();
                    Cursor cr = dbs.rawQuery("SELECT * FROM personalinfo WHERE " + EMAIL + " = ? ", new String[]{emails.getEditText().getText().toString().trim()});
                    cr.moveToFirst();
                    if (cr.getCount()>0) {
                        if (!(cr.getString(cr.getColumnIndex(PASS)).equals(passw.getEditText().getText().toString()))) {
                            String s = cr.getString(cr.getColumnIndex(PASS));
                            Toast.makeText(getActivity(), s, Toast.LENGTH_SHORT).show();
                            passw.setError("Incorrect Password");
                        } else {
                            ac.setEmails(emails.getEditText().getText().toString().trim());
                            ac.setIds(cr.getInt(cr.getColumnIndex(ID)));
                            ac.setPass(passw.getEditText().getText().toString().trim());

                            Toast.makeText(getActivity(), "Welcome!", Toast.LENGTH_SHORT).show();
                            Fragment fragment = new home();
                            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.frag_container, fragment);
                            fragmentTransaction.commit();

                            NavigationView navigationView = (NavigationView) getActivity().findViewById(R.id.nav_view);
                            Menu nav_Menu = navigationView.getMenu();
                            navigationView.setCheckedItem(R.id.home);
                            nav_Menu.findItem(R.id.profile).setVisible(true);
                            nav_Menu.findItem(R.id.cd).setVisible(true);
                            nav_Menu.findItem(R.id.signout).setVisible(true);
                            nav_Menu.findItem(R.id.signin).setVisible(false);
                        }
                    }else {
                        Toast.makeText(getActivity(), "Please Sign up!", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

        Button button = (Button)v.findViewById(R.id.button1);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // When button is clicked, call up to owning activity.
                ImageView cls;
                md.setContentView(R.layout.signup);
                cls = (ImageView) md.findViewById(R.id.cls);
                cls.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        md.dismiss();
                    }
                });
                md.show();

                textEmail = md.findViewById(R.id.editText);
                textpass = md.findViewById(R.id.editText2);
                textpass1 = md.findViewById(R.id.editText3);
                et=md.findViewById(R.id.email);
                et1=md.findViewById(R.id.pass);

                signupbut=md.findViewById(R.id.signups);
                signupbut.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!validateEmail() | !validatePass()) {
                            return;
                        } else {
                            SQLiteDatabase dbs = mdb.getWritableDatabase();
                            Cursor cr = dbs.rawQuery("SELECT * FROM personalinfo WHERE " + EMAIL + " = ?", new String[]{et.getText().toString()});
                            cr.moveToFirst();
                            if (cr.getCount() > 0) {
                                Toast.makeText(getActivity(), "Email Exists!", Toast.LENGTH_SHORT).show();
                                textEmail.setError("Email Exists!");
                            } else {
                                boolean isinserted = mdb.insertsign(et.getText().toString(), et1.getText().toString());

                                if (isinserted == true) {
                                    cr.moveToFirst();

                                    ac.setEmails(et.getText().toString().trim());
                                    ac.setPass(et1.getText().toString());

                                    Fragment fragment = new home();
                                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                    fragmentTransaction.replace(R.id.frag_container, fragment);
                                    fragmentTransaction.commit();

                                    NavigationView navigationView = (NavigationView) getActivity().findViewById(R.id.nav_view);
                                    Menu nav_Menu = navigationView.getMenu();
                                    navigationView.setCheckedItem(R.id.home);
                                    nav_Menu.findItem(R.id.profile).setVisible(true);
                                    nav_Menu.findItem(R.id.cd).setVisible(true);
                                    nav_Menu.findItem(R.id.signout).setVisible(true);
                                    nav_Menu.findItem(R.id.signin).setVisible(false);
                                    md.dismiss();
                                    Toast.makeText(getActivity(), "Signed Up Successfully!", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getActivity(), "Sign Up Failed!", Toast.LENGTH_SHORT).show();
                                }

                            }
                        }
                    }
                });

            }
        });
        return v;

    }


    private void fetchUserInfo() {
        final String PERMISSION = "publish_actions";
        final AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null) {
            GraphRequest request = GraphRequest.newMeRequest(
                    accessToken,
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            // Insert your code here
                            parseUserInfo(object);
                        }
                    });

            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email");
            request.setParameters(parameters);
            request.executeAsync();

            LoginManager.getInstance().logInWithPublishPermissions(
                    getActivity(),
                    Arrays.asList(PERMISSION));
        } else {
            Toast.makeText(getActivity(), "Not in!", Toast.LENGTH_LONG).show();

        }
    }

    private void parseUserInfo(JSONObject me) {



        try {

            id = me.getString("id");
            name = me.getString("name");
            email = me.getString("email");

            ac.setNames(name);
            ac.setEmails(email);

            SQLiteDatabase dbs = mdb.getWritableDatabase();
            Cursor cr = dbs.rawQuery("SELECT * FROM personalinfo WHERE " + EMAIL + " = ? ", new String[]{email});
            cr.moveToFirst();
            if (cr.getCount()<=0) {
                boolean isinserted = mdb.insertsign(email, " ");
            }

        } catch (JSONException e) {
            e.printStackTrace();

        }


    }



    @Override
    public void onStart() {
        super.onStart();
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(getActivity());
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (resultCode == RESULT_OK) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            if (requestCode == RC_SIGN_IN) {
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                handleSignInResult(task);
            }else{
                //If not request code is RC_SIGN_IN it must be facebook
                callbackManager.onActivityResult(requestCode, resultCode, data);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            Fragment fragment = new home();
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frag_container, fragment);
            fragmentTransaction.commit();
            NavigationView navigationView = (NavigationView) getActivity().findViewById(R.id.nav_view);
            Menu nav_Menu = navigationView.getMenu();
            navigationView.setCheckedItem(R.id.home);
            nav_Menu.findItem(R.id.profile).setVisible(true);
            nav_Menu.findItem(R.id.cd).setVisible(true);
            nav_Menu.findItem(R.id.signout).setVisible(true);
            nav_Menu.findItem(R.id.signin).setVisible(false);

            Toast.makeText(getContext(),"Signed in with google, Welcome!",Toast.LENGTH_LONG).show();

        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("Error", "signInResult:failed code=" + e.getStatusCode());
        }
    }


    private boolean validateEmail() {
        String emailInput = textEmail.getEditText().getText().toString().trim();
        if (emailInput.isEmpty()) {
            textEmail.setError("Field can't be empty");
            return false;
        } else {
            textEmail.setError(null);
            return true;
        }
    }
    private boolean validateEmails() {
        String emailInput = emails.getEditText().getText().toString().trim();
        if (emailInput.isEmpty()) {
            emails.setError("Field can't be empty");
            return false;
        } else {
            emails.setError(null);
            return true;
        }
    }
    private boolean validatePassw() {
        String emailInput = passw.getEditText().getText().toString().trim();
        if (emailInput.isEmpty()) {
            passw.setError("Field can't be empty");
            return false;
        } else {
            passw.setError(null);
            return true;
        }
    }
    private boolean validatePass() {
        String passwordInput = textpass.getEditText().getText().toString().trim();
        String passinput = textpass1.getEditText().getText().toString().trim();
        if (passwordInput.isEmpty() && passinput.isEmpty()) {
            textpass.setError("Field can't be empty");
            textpass1.setError("Field can't be empty");
            return false;
        }else if (!passinput.contentEquals(passwordInput)){
            textpass.setError("Password not same");
            textpass1.setError("Password not same");
            return false;
        }else{
            textpass.setError(null);
            textpass1.setError(null);
            return true;
        }
    }

//    public void signup(View vs){
//        ImageView cls;
//        md.setContentView(R.layout.signup);
//        cls = (ImageView) getView().findViewById(R.id.cls);
//        cls.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                md.dismiss();
//            }
//        });
//        md.show();
//    }

}

