package com.example.goutham.solo;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

public class devices extends Fragment {
    public static int asdf=0,as1=0,as2=0,asd=0;

    Dialog md;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.device,container,false);
        md=new Dialog(getActivity());

        Button b,b1,b2,b3;
        b=v.findViewById(R.id.bulb);
        b1=v.findViewById(R.id.bulb1);
        b2=v.findViewById(R.id.bulb2);
        b3=v.findViewById(R.id.bulb3);

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(asdf==1) {
                    v.setBackgroundResource(R.drawable.lightbulb_on);
                    asdf=0;
                }else {
                    v.setBackgroundResource(R.drawable.lightbulb_off);
                    asdf=1;
                }
            }
        });

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(asd==1) {
                    v.setBackgroundResource(R.drawable.lightbulb_on);
                    asd=0;
                }else {
                    v.setBackgroundResource(R.drawable.lightbulb_off);
                    asd=1;
                }
            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(as1==1) {
                    v.setBackgroundResource(R.drawable.lightbulb_on);
                    as1=0;
                }else {
                    v.setBackgroundResource(R.drawable.lightbulb_off);
                    as1=1;
                }
            }
        });

        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(as2==1) {
                    v.setBackgroundResource(R.drawable.lightbulb_on);
                    as2=0;
                }else {
                    v.setBackgroundResource(R.drawable.lightbulb_off);
                    as2=1;
                }
            }
        });

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.bulb:
                        if(asdf==0){
                            v.setBackgroundResource(R.drawable.lightbulb_off);
                            asdf=1;
                        }else {
                            v.setBackgroundResource(R.drawable.lightbulb_on);
                            asdf=0;
                        }
                        break;
                    case R.id.bulb1:
                        if(asd==0){
                            v.setBackgroundResource(R.drawable.lightbulb_off);
                            asd=1;
                        }else {
                            v.setBackgroundResource(R.drawable.lightbulb_on);
                            asd=0;
                        }
                        break;
                    case R.id.bulb2:
                        if(as1==0){
                            v.setBackgroundResource(R.drawable.lightbulb_off);
                            as1=1;
                        }else {
                            v.setBackgroundResource(R.drawable.lightbulb_on);
                            as1=0;
                        }
                        break;
                    case R.id.bulb3:
                        if(as2==0){
                            v.setBackgroundResource(R.drawable.lightbulb_off);
                            as2=1;
                        }else {
                            v.setBackgroundResource(R.drawable.lightbulb_on);
                            as2=0;
                        }
                        break;
                }
            }
        });

        FloatingActionButton fab= v.findViewById(R.id.floatingActionButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView cls;
                md.setContentView(R.layout.fab_contact);
                cls = (ImageView) md.findViewById(R.id.cls);
                cls.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        md.dismiss();
                    }
                });
                Button btn =md.findViewById(R.id.button);
                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        intent.putExtra(Intent.EXTRA_TEXT, "Please change the current plan of " + ActiveUser.getEmails()+" "+ActiveUser.getNames());
                        if(intent.resolveActivity( getActivity().getPackageManager() )!=null)
                            startActivity(intent);

                    }
                });
                md.show();
            }
        });
        return v;
    }

}
