package com.example.goutham.solo;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class product extends Fragment {

    Dialog md;
    Animation fades;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.product,container,false);
        md=new Dialog(getActivity());
        fades=AnimationUtils.loadAnimation(getActivity(),R.anim.fadeanimd);


        TextView b = (TextView) v.findViewById(R.id.buy);
        b.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                md.getWindow().getAttributes().windowAnimations = R.anim.fadeanimd;
                // When button is clicked, call up to owning activity.
                ImageView cls;
                md.setContentView(R.layout.buy);
                cls = (ImageView) md.findViewById(R.id.cls);
                cls.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        md.dismiss();
                    }
                });
                md.show();

            }
        });

        TextView b1 = (TextView) v.findViewById(R.id.buy2);
        b1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // When button is clicked, call up to owning activity.
                md.getWindow().getAttributes().windowAnimations = R.anim.fadeanimd;

                ImageView cls;
                md.setContentView(R.layout.buy1);
                cls = (ImageView) md.findViewById(R.id.cls);
                cls.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        md.dismiss();
                    }
                });
                md.show();

            }
        });

        TextView b2 = (TextView) v.findViewById(R.id.buy3);
        b2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // When button is clicked, call up to owning activity.
                md.getWindow().getAttributes().windowAnimations = R.anim.fadeanimd;

                ImageView cls;
                md.setContentView(R.layout.buy2);
                cls = (ImageView) md.findViewById(R.id.cls);
                cls.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        md.dismiss();
                    }
                });
                md.show();

            }
        });


        TextView tv = (TextView) v.findViewById(R.id.sm1);
        tv.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // When button is clicked, call up to owning activity.
                md.getWindow().getAttributes().windowAnimations = R.anim.fadeanimd;

                ImageView cls;
                md.setContentView(R.layout.p1);
                cls = (ImageView) md.findViewById(R.id.cls);
                cls.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        md.dismiss();
                    }
                });
                md.show();

            }
        });

        TextView tv1 = (TextView) v.findViewById(R.id.sm2);
        tv1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // When button is clicked, call up to owning activity.
                md.getWindow().getAttributes().windowAnimations = R.anim.fadeanimd;

                ImageView cls;
                md.setContentView(R.layout.p2);
                cls = (ImageView) md.findViewById(R.id.cls);
                cls.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        md.dismiss();
                    }
                });
                md.show();

            }
        });

        TextView tv2 = (TextView) v.findViewById(R.id.sm3);
        tv2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // When button is clicked, call up to owning activity.
                md.getWindow().getAttributes().windowAnimations = R.anim.fadeanimd;

                ImageView cls;
                md.setContentView(R.layout.p3);
                cls = (ImageView) md.findViewById(R.id.cls);
                cls.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        md.dismiss();
                    }
                });
                md.show();

            }
        });


        return v;
    }

//    public void signup(View vs){
//        ImageView cls;
//        md.setContentView(R.layout.signup);
//        cls = (ImageView) getView().findViewById(R.id.cls);
//        cls.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                md.dismiss();
//            }
//        });
//        md.show();
//    }

}

