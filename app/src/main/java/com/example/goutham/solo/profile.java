package com.example.goutham.solo;

import android.app.Dialog;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.VideoView;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class profile extends Fragment {
    Dialog md;
    private TextInputLayout textInputEmail,textInputUsername;
    private TextInputLayout textInputName,textInputAddress,textInputConctact;
    private TextInputLayout textInputPassword,textInputDob;
    private Button bt,edit;
    ActiveUser ac = new ActiveUser();
    DatabaseHelper mdb;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.profile,container,false);
        textInputEmail = v.findViewById(R.id.text_input_email);
        textInputUsername = v.findViewById(R.id.text_input_username);
        textInputPassword = v.findViewById(R.id.text_input_password);
        textInputAddress = v.findViewById(R.id.text_input_address);
        textInputConctact = v.findViewById(R.id.text_input_contact);
        textInputDob = v.findViewById(R.id.text_input_dob);
        textInputName = v.findViewById(R.id.text_input_name);


        bt=v.findViewById(R.id.confirm);
        edit=v.findViewById(R.id.edit);
        mdb = new DatabaseHelper(getActivity());
        md=new Dialog(getActivity());

        textInputConctact.getEditText().setEnabled(false);
        textInputDob.getEditText().setEnabled(false);
        textInputAddress.getEditText().setEnabled(false);
        textInputName.getEditText().setEnabled(false);
        textInputEmail.getEditText().setEnabled(false);
        textInputPassword.getEditText().setEnabled(false);
        textInputUsername.getEditText().setEnabled(false);

        Cursor cr = mdb.getProfile();
        cr.moveToFirst();
        textInputName.getEditText().setText(cr.getString(1));
        textInputEmail.getEditText().setText(cr.getString(2));
        textInputUsername.getEditText().setText(cr.getString(3));
        textInputAddress.getEditText().setText(cr.getString(4));
        textInputConctact.getEditText().setText(cr.getString(5));
        textInputDob.getEditText().setText(cr.getString(6));


        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ImageView cls;
                md.setContentView(R.layout.disclaimer);
                cls = (ImageView) md.findViewById(R.id.cls);
                cls.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        md.dismiss();
                        textInputConctact.getEditText().setEnabled(false);
                        textInputDob.getEditText().setEnabled(false);
                        textInputAddress.getEditText().setEnabled(false);
                        textInputName.getEditText().setEnabled(false);
                        textInputEmail.getEditText().setEnabled(false);
                        textInputPassword.getEditText().setEnabled(false);
                        textInputUsername.getEditText().setEnabled(false);
                    }
                });
                md.show();
                Button conf = md.findViewById(R.id.disconf);

                conf.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        textInputConctact.getEditText().setEnabled(true);
                        textInputDob.getEditText().setEnabled(true);
                        textInputAddress.getEditText().setEnabled(true);
                        textInputName.getEditText().setEnabled(true);
                        textInputEmail.getEditText().setEnabled(false);
                        textInputPassword.getEditText().setEnabled(true);
                        textInputUsername.getEditText().setEnabled(true);
                        md.dismiss();
                    }
                });
            }
        });

        bt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                boolean isinserted=false;
                if (!validateEmail() | !validateUsername() | !validateName() |
                        !validateAddress() | !validateContact() | !validateDob()) {
                    return;
                }else{
                    try {
                        String s =textInputPassword.getEditText().getText().toString().trim();
                        if(( s.isEmpty()|| s.equals("")||s.equals(null)|TextUtils.isEmpty(s)||s==null ) )
                            s=ActiveUser.getPass();
                        isinserted = mdb.insertProfile(textInputUsername.getEditText().getText().toString().trim(),
                                            textInputAddress.getEditText().getText().toString().trim(),
                                            textInputConctact.getEditText().getText().toString(),
                                            textInputName.getEditText().getText().toString().trim(),
                                            textInputDob.getEditText().getText().toString().trim(),
                                            s);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (isinserted == true) {
                        ac.setUsername(textInputUsername.getEditText().getText().toString().trim());
                        ac.setAddress(textInputAddress.getEditText().getText().toString().trim());
                        ac.setContact(textInputConctact.getEditText().getText().toString());
                        ac.setNames(textInputName.getEditText().getText().toString().trim());
                        ac.setDob(textInputDob.getEditText().getText().toString().trim());
                        if ((textInputPassword.getEditText().getText().toString().trim().isEmpty())) {
                            ac.setPass(textInputPassword.getEditText().getText().toString().trim());
                        }
                    }else {
                        Toast.makeText(getActivity(), "Adding details Failed!", Toast.LENGTH_SHORT).show();
                    }

                }



                textInputConctact.getEditText().setEnabled(false);
                textInputDob.getEditText().setEnabled(false);
                textInputAddress.getEditText().setEnabled(false);
                textInputName.getEditText().setEnabled(false);
                textInputEmail.getEditText().setEnabled(false);
                textInputPassword.getEditText().setEnabled(false);
                textInputUsername.getEditText().setEnabled(false);
            }
        });
        return v;

    }
    private boolean validateEmail() {
        String emailInput = textInputEmail.getEditText().getText().toString().trim();
        if (emailInput.isEmpty()) {
            textInputEmail.setError("Field can't be empty");
            return false;
        } else {
            textInputEmail.setError(null);
            return true;
        }
    }

    private boolean validateDob() {
        String emailInput = textInputDob.getEditText().getText().toString().trim();
        if (emailInput.isEmpty()) {
            textInputDob.setError("Field can't be empty");
            return false;
        } else {
            textInputDob.setError(null);
            return true;
        }
    }

    private boolean validateContact() {
        String emailInput = textInputConctact.getEditText().getText().toString().trim();
        if (emailInput.isEmpty()) {
            textInputConctact.setError("Field can't be empty");
            return false;
        } else {
            textInputConctact.setError(null);
            return true;
        }
    }

    private boolean validateUsername() {
        String usernameInput = textInputUsername.getEditText().getText().toString().trim();
        if (usernameInput.isEmpty()) {
            textInputUsername.setError("Field can't be empty");
            return false;
        } else if (usernameInput.length() > 15) {
            textInputUsername.setError("Username too long");
            return false;
        } else {
            textInputUsername.setError(null);
            return true;
        }
    }

    private boolean validateAddress() {
        String usernameInput = textInputAddress.getEditText().getText().toString().trim();
        if (usernameInput.isEmpty()) {
            textInputAddress.setError("Field can't be empty");
            return false;
        } else if (usernameInput.length() > 150) {
            textInputAddress.setError("Address too long");
            return false;
        } else {
            textInputAddress.setError(null);
            return true;
        }
    }

    private boolean validateName() {
        String usernameInput = textInputName.getEditText().getText().toString().trim();
        if (usernameInput.isEmpty()) {
            textInputName.setError("Field can't be empty");
            return false;
        } else if (usernameInput.length() > 15) {
            textInputName.setError("Name too long");
            return false;
        } else {
            textInputName.setError(null);
            return true;
        }
    }

    private boolean validatePassword() {
        String passwordInput = textInputPassword.getEditText().getText().toString().trim();
        if (passwordInput.isEmpty()) {
            textInputPassword.setError("Field can't be empty");
            return false;
        } else {
            textInputPassword.setError(null);
            return true;
        }
    }

}